package esilv.sdp.java.td01.ex01;

import up.mi.jgm.maths.Complexe;
import up.mi.jgm.maths.Rationnel;

public class UtilRatCom {

    public static Rationnel nPowerOfRationalNumber(Rationnel number, int nPower) {
        Rationnel stockage = new Rationnel(0);
        for (int i = 1; i < nPower; i++) {
            stockage = number.multiplication(number);
        }
        return stockage;
    }

    public static Complexe sum(Complexe a, Complexe b){
        Complexe temp = new Complexe(a.getPartieReelle() + b.getPartieReelle(),
                a.getPartieImaginaire() + b.getPartieImaginaire());
        return temp;
    }

    public static Complexe mult(Complexe a, Complexe b){
        Complexe temp = new Complexe (a.getPartieReelle()*b.getPartieReelle()-a.getPartieImaginaire()* b.getPartieImaginaire()
                ,a.getPartieReelle()*b.getPartieImaginaire()+a.getPartieImaginaire()*b.getPartieReelle());
        return temp;
    }

}