package esilv.sdp.java.td01.ex01;

public class UtilMath {
    public static int somme3(int a, int b, int c) {
        return a + b + c;
    }

    public static long fact(int n) {
        int fact=1;
        for (int i = 1; i <= n; i++) {
            fact = fact * i;
        }
        return fact;
    }

    public static long puissance(int n, int m) {
        long result=1;
        while (m!=0){
            result*=n;
            --m;
        }
        return result;
    }

    public static long comb1(int n, int p) {
        if (p > n - p) p = n - p;
        long result = 1;
        for (int i = 1, m = n; i <= p; i++, m--) {
            result = result * m / i;
        }
        return result;
    }
}