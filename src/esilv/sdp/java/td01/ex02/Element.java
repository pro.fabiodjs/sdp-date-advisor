package esilv.sdp.java.td01.ex02;

import java.util.Collections;
import java.util.List;

public class Element implements Comparable<Element>{
    private String name;
    public Element(String name) {
        this.name = name;
    }

    public String name() {
        return name;
    }

    @Override
    public String toString(){
        return this.name;
    }


    @Override
    public int compareTo(Element element) {
        return this.name.compareTo(element.name);
    }
}
