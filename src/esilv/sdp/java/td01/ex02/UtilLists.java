package esilv.sdp.java.td01.ex02;

import javax.lang.model.element.Element;
import java.util.Collections;
import java.util.List;

public class UtilLists {
    public static Integer maximumList (List<Integer> list){
        return Collections.max(list);
    }

    public static List<Integer> listSorted (List<Integer> list){
        Collections.sort(list);
        return list;
    }

    


}
