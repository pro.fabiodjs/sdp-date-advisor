package esilv.sdp.java.td01.ex03;

public class Disk extends  Figure implements Sizeable{
    double rayon ;

    public Disk(double rayon) {
        this.rayon = rayon;
    }

    @Override
    public double perimeter() {
        return (2*Math.PI*rayon) ;
    }

    @Override
    public double surface() {
        return (Math.PI * rayon * rayon) ;
    }

    @Override
    public Figure translate(Vector v) {
        return null;
    }

    public String compareFigures(Object o){
        if(o.getClass() == Disk.class){
            if(this.perimeter() >= ((Disk) o).perimeter() && this.surface() >= ((Disk) o).surface()){
                return "Le disque comparant est le plus grand" ;
            }
            else{
                return "Le disque comparé est le plus grand" ;
            }
        }
        else{
            return "On ne peut pas comparer un disque et un cercle" ;
        }
    }
}
