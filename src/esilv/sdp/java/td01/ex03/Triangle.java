package esilv.sdp.java.td01.ex03;

public class Triangle extends  Figure implements Sizeable{
    double x ;
    double y ;
    double z ;

    public Triangle(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;

    }

    @Override
    public double perimeter() {
        return x+y+z ;
    }

    @Override
    public double surface() {
        double p = (x + y + z)/2 ;
        return  Math.pow((p*(p-x)*(p-y)*(p-z)) , (0.5)) ;
    }

    @Override
    public Figure translate(Vector v) {
        return null;
    }

    public String compareFigures(Object o){
        if(o.getClass() == Triangle.class){
           if(this.perimeter() >= ((Triangle) o).perimeter() && this.surface() >= ((Triangle) o).surface()){
               return "Le triangle comparant est le plus grand" ;
           }
           else{
               return "Le triangle comparé est le plus grand" ;
           }
        }
        else{
            return "On ne peut pas comparer un disque et un cercle" ;
        }
    }
}
