import esilv.sdp.java.td01.ex01.UtilMath;
import esilv.sdp.java.td01.ex01.UtilRatCom;
import esilv.sdp.java.td01.ex02.Element;
import esilv.sdp.java.td01.ex02.UtilLists;
import esilv.sdp.java.td01.ex03.Disk;
import esilv.sdp.java.td01.ex03.Triangle;
import up.mi.jgm.maths.Complexe;
import up.mi.jgm.maths.Rationnel;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Exercice 1: Calculations

        System.out.println("Exercice 1 : ");


        int a = UtilMath.somme3(1,2,3);
        System.out.println(a);
        long b = UtilMath.fact(4);
        System.out.println(b);
        long c = UtilMath.comb1(10,2);
        System.out.println(c);
        long d = UtilMath.puissance(3,2);
        System.out.println(d);

        // Exercice 1 petit 4 :
        Rationnel e = new Rationnel(3,8);
        Rationnel f = UtilRatCom.nPowerOfRationalNumber(e, 5) ;
        System.out.println(f);

        Complexe g = new Complexe (4,5);
        Complexe h = UtilRatCom.sum(g,g);
        System.out.println(h.toString());

        Complexe i = new Complexe (4,5);
        Complexe j = UtilRatCom.mult(i,i);
        System.out.println(j.toString());

        // Fin de l'exercice 1

        // Exercice 2: Lists

        System.out.println("Exercice 2 : ");

        List<Integer> list=new ArrayList<Integer>(){{
            add(1);
            add(2);
            add(3);
            add(3);
            add(37444447);
            add(8);
            add(3254);
            add(374);

        }};
        System.out.println(UtilLists.maximumList(list));
        UtilLists.listSorted(list);
        System.out.println(list);


        Element element1 = new Element("coucou");

        Element element2 = new Element("salut");

        Element element3 = new Element("aaaa");


        List<Element> listElement = new ArrayList<Element>() ;
        listElement.add(element1) ;
        listElement.add(element2);
        listElement.add(element3);
        System.out.println(listElement);

        Collections.sort(listElement);

        System.out.println(listElement);

        // Fin de l'exercice 2

        // Exercice 3: Inheritance, Interface and Abstract class

        Triangle triangle = new Triangle(5.0 , 6.0 , 3.0);
        Triangle triangle2 = new Triangle(6.0 , 3.0 , 6.0);

        Disk disk = new Disk(4.0) ;
        Disk disk2 = new Disk(8.0) ;

        System.out.println("Exercice 3 : ");

        System.out.println(triangle.perimeter());
        System.out.println(triangle.surface());
        System.out.println(triangle2.perimeter());
        System.out.println(triangle2.surface());
        System.out.println(disk.perimeter());
        System.out.println(disk.surface());
        System.out.println(disk2.perimeter());
        System.out.println(disk2.surface());


        System.out.println(triangle.compareFigures(disk));
        System.out.println(triangle.compareFigures(triangle2));
        System.out.println(disk2.compareFigures(disk));


        //Fin de l'exercice 3

        // Exercice 4: Scrum game



        // Fin de l'exercice 4
    }
}